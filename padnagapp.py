#!/usr/bin/env python3
"""
Padnag.

Usage:
  padnag  [--config CONFIGFILE] [--poll seconds] [--test] [--verbose] [--force] [--about] [--key LICENSE_KEY_FILE]
  padnag  [-c CONFIGFILE] [-p seconds] [-tvf] [-p <seconds>]
  padnag (-h | --help)
  padnag --version

Synchronize PostgreSQL database roles with Active Directory groups & group members.

Options:
  -c CONFIGFILE --config CONFIGFILE  Optional configuration file [default: padnag.toml]
  -p <seconds> --poll <seconds>      Poll the Active Directory every n seconds
  -t --test                          Only print the intended database actions, don't execute them
  -v --verbose                       Show detailed output. (Not all SQL, just choice statements)
  -f --force                         Ignore transform errors and proceed
     --key LICENSE_KEY_FILE          Optional license key file [default: license.key]
     --help                          Show this screen
     --version                       Show version
     --about                         Show license information


Examples:
    padnag
    padnag --test --verbose
    padnag --config production.toml --verbose --poll 59
"""

from typing import Any, Dict, Union
import logging
import os
import sys
from time import sleep

import colorlog  # GSTQ
from docopt import docopt
from padnag.settings import Settings, InvalidConfigException
from padnag.licensing import Licensing, UnlicensedException, UncommencedLicenseException, ExpiredLicenseException
from truepy import LicenseData

from padnag.transformer import TransformException
from ldap3.core.exceptions import LDAPExceptionError, LDAPInvalidFilterError
import pg8000



class DispatchingFormatter(logging.Formatter):
    """
    Dispatch formatter for logger and it's sub logger.
    Thanks: http://stackoverflow.com/a/34626685/266387
    """
    def __init__(self, formatters: Dict[str, logging.Formatter], default_formatter: logging.Formatter) -> None:
        self._formatters = formatters
        self._default_formatter = default_formatter

    def format(self, record: Any) -> str:
        # Search from record's logger up to it's parents:
        logger: Any = logging.getLogger(record.name)
        while logger:
            # Check if suitable formatter for current logger exists:
            if logger.name in self._formatters:
                formatter = self._formatters[logger.name]
                break
            else:
                logger = logger.parent
        else:
            # If no formatter found, just use default:
            formatter = self._default_formatter
        return formatter.format(record)


def main() -> int:
    ### BEGIN NB This next line is edited by the tag_release.sh and bumpversion. Don't break the regex
    arguments: Dict[str, Any] = docopt(__doc__, version='PADNAG 1.2.2')
    ### END NB
    logging.captureWarnings(True)
    log = logging.getLogger(__name__)
    log.debug('command line arguments: %s' % str(arguments))

    try:
        license_data: LicenseData = Licensing().validate(arguments['--key'])
        if arguments.get('--about', None):
            log.info('This software is licensed to {0}, from {1} until {2}'.format(license_data.info,
                                                                                   license_data.not_before,
                                                                                   license_data.not_after))
            log.debug('License issued:{0}'.format(license_data.issued))
            log.debug('License issuer:{0}'.format(license_data.issuer))
            log.debug('License holder:{0}'.format(license_data.holder))
            log.debug('License extra:{0}'.format(license_data.extra))
            sys.exit(0)

    except UnlicensedException as ex:
        log.warning('This is an unlicensed version. Copyright padnag.io 2017. All rights reserved.')
        if arguments.get('--about', None):
            sys.exit(13)
    except (UncommencedLicenseException, ExpiredLicenseException) as ex:
        log.warning('This license is only valid from {0} until {1}'.format(ex.not_before, ex.not_after))
        log.debug('License validity tested as {0}'.format(ex.tested_at))
        log.warning('This is an unlicensed version. Copyright padnag.io 2017. All rights reserved.')
        if arguments.get('--about', None):
            sys.exit(14)
    except FileNotFoundError as fex:
        log.warning('An error occurred while trying to read the license key "{0}".'.format(arguments['--key']))
        log.warning('This is an unlicensed version. Copyright padnag.io 2017. All rights reserved.')
        log.debug(str(fex))
        if arguments.get('--about', None):
            sys.exit(15)
    except Exception as oex:
        log.warning('An error occurred while trying to validate the license.')
        log.warning('This is an unlicensed version. Copyright padnag.io 2017. All rights reserved.')
        log.debug(str(oex))
        if arguments.get('--about', None):
            sys.exit(16)


    overrides = {
        'General':
            {
                'verbose': arguments['--verbose'],
                'test'   : arguments['--test'],
                'force'  : arguments['--force'],
                'poll'   : arguments['--poll'],
            }
    }
    if len(arguments['--poll']):
        overrides['General']['poll'] = int(arguments['--poll'][0])
    else:
        overrides['General']['poll'] = 0

    settings = Settings()

    if getattr(sys, 'frozen', False) and arguments['--config'] == 'padnag.toml':
        # we are running in a pyinstaller frozen bundle
        executable_path = os.path.dirname(sys.executable)
        arguments['--config'] = os.path.join(executable_path, 'padnag.toml')

    try:
        settings.load_ini_file(arguments['--config'], overrides)
    except FileNotFoundError as ex:
        log.critical('Critical error {0}. Terminating now.'.format(ex.args[0]))
        sys.exit(1)
    except InvalidConfigException as ex:
        log.critical('Critical error {0}. Terminating now.'.format(ex.args[0]))
        sys.exit(2)
    except TransformException as ex:
        if not arguments['--force']:
            log.critical('Error with settings while parsing transform. Terminating now.')
            sys.exit(3)
        else:
            # already wraned with details
            log.warning('Error with settings. Will attempt to ignore because of --force.')
    except ValueError as ex:
        log.critical('Critical value error with settings ( Possibly database port). Terminating now.')
        sys.exit(5)

    except Exception as ex:
        log.exception('Unexpected exception occurred. Terminating now.')
        sys.exit(9)


    # These imports might throw errors so do them as late as possible
    from padnag.database import DBConn
    from padnag.directoryservice import ADConnection
    DBConn.conn_kwargs = settings.ini_db
    ADConnection.conn_kwargs = settings.ini_ad

    try:
        with DBConn() as test_connection:
            db = DBLogic(test_connection, test_mode=arguments['--test'], verbose=arguments['--verbose'])
            # test the database connection and privileges
            try:
                db.check_alive()
            except Exception as ex:
                log.critical('Critical Error. Basic database connection check failed.')
                sys.exit(6)

            if not  db.check_program_privileges():
                if arguments['--test']:
                    log.warning('Warning. Insufficient database privileges.')
                else:
                    log.critical('Warning. Insufficient database privileges. Terminating now.')
                    sys.exit(8)
                test_connection.close()
    except pg8000.InterfaceError as ex:
        log.debug("Database connection refused or mis-configured SSL.")
        raise ex
    except Exception as ex:
        log.critical('Critical Database Error. export LOG_LEVEL=DEBUG for more information')
        sys.exit(6)

    try:
        with ADConnection() as test_ad:
            pass
    except Exception as ex:
        log.critical('Critical Error. Could not connect to the Active Directory. export LOG_LEVEL=DEBUG for more information')
        sys.exit(7)

    engine = Engine(settings.ini_organisations,
                    settings.ini_flat_roles,
                    settings.ini_group_transforms,
                    settings.ini_role_transforms,
                    test_mode=arguments['--test'],
                    verbose=arguments['--verbose']
                    )
    try:
        engine.sync()
        if arguments['--poll']:
            try:
                seconds = int(arguments['--poll'][0])
                while True:
                    logging.debug('Sleeping')
                    sleep(seconds)
                    log.info('Running sync')
                    engine.sync()
            except Exception as ex:
                logging.critical('Configuration error; -p or --poll must provide a seconds argument. Terminating now.')
                sys.exit(3)
    except LDAPInvalidFilterError:
        sys.exit(10)
    except LDAPExceptionError:
        sys.exit(11)
    except Exception:
        sys.exit(12)
    return 0


if __name__ == '__main__':
    logger = logging.getLogger('')
    handler = logging.StreamHandler()
    formatter = DispatchingFormatter({
        # 'some.module': logging.Formatter('%(message)s -> one'),
        'padnag.database.sql': colorlog.ColoredFormatter(
                            '{log_color}{levelname:8} [{asctime}] SQL{reset}{sql_log_color}{message}',
                            datefmt="%Y-%m-%d %H:%M:%S",
                            style='{',
                            secondary_log_colors={
                                'sql': {
                                    'INFO' : 'cyan',
                                    'DEBUG': 'cyan'
                                }
                            }
                        ),
        },
        colorlog.ColoredFormatter('{log_color}{levelname:8} [{asctime}] {message}',
                                  datefmt="%Y-%m-%d %H:%M:%S",
                                  style='{',
                                ),
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    try:
        logger.setLevel(getattr(logging, os.environ.get("LOG_LEVEL", "INFO").upper().strip()))
    except Exception as ex:
        logger.warning('Invalid LOG_LEVEL, defaulting to "INFO"')
        logger.setLevel(logging.INFO)

    from padnag.database import DBLogic
    from padnag.engine import Engine
    from padnag.settings import Settings, InvalidConfigException

    sys.exit(main())

