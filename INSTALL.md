
Centos 7 
--------
* Download and install the RPM file 
* Edit the database and active directory settings in the config file
* Test run from the comand line

~~~
sudo yum install -y padnag-1.1-1.el7.centos.x86_64.rpm
# edit and rename /opt/padnag/bin/padnag.toml
cd /opt/padnag/bin/
nano padnag.toml
~~~

Test run from the command line
~~~

cd /opt/padnag/bin/
./padnag --config padnag.toml --test --verbose
#
sudo systemctl start padnag
sudo systemctl enable padnag
~~~
