from typing import Any, Dict, List, Set
import inflect

from padnag.directoryservice import ADConnection, ADLogic
from padnag.database import DBConn, DBLogic, PGRType
from padnag.transformer import TransformException, Transform
from padnag.settings import SettingsOrganisation, SettingsFlatRole
import warnings
import logging
from ldap3.core.exceptions import LDAPExceptionError
log = logging.getLogger(__name__)

# for pluralizing
pluraliser = inflect.engine()


class Engine(object):
    def __init__(self,
                 organisations: List[SettingsOrganisation],
                 flat_roles: List[SettingsFlatRole],
                 group_transforms: List[Transform],
                 role_transforms: List[Transform],
                 test_mode: bool=False,
                 verbose=False) -> None:

        self.organisations = organisations
        self.flat_roles = flat_roles
        self.group_transforms = group_transforms
        self.role_transforms = role_transforms
        self.test_mode = test_mode
        self.verbose = verbose


    def sync(self) -> None:
        """
        ADConnection must be already set up.
        ADConnection.conn_kwargs = self.ini_ad
        
        Postgres connections can't "USE dbname;" so it's not possible to to everything in a single db txn.
        This comes into play trying to drop roles that own objects or have privileges in other catalogs in the cluster.
        
        Solution is to:
          *  Gather information about present & desired state   
          *  Start the main db txn
          *  CREATE all the required groups and roles
          *  REVOKE all extraneous memberships 
          *  REASSIGN OWNER for all objects that are going to be dropped in current db.
          *  Try and DROP obsolete roles and groups, ignoring errors
          *  Reject up managed roles that have been fiddled with
          *  COMMIT current db txn.  
          *  REASSIGN all objects in other catalogs
          *  DROP any remaining obsolete roles and groups 
  
        
        
        :return:
        """
        with DBConn() as db_conn:
            db = DBLogic(
                db_conn,
                test_mode=self.test_mode,
                verbose=self.verbose
            )
            ad = ADLogic()

            # target_memberships_dict is a dictionary keyed on the group __OR__ role name;
            # valued with a set of groups that key belongs to

            # target_groups_dict is a dictionary keyed by group where each value is the set of direct group "members"
            # which could be role or group names.
            # as such, target_groups_dict.keys() is a list of all the group roles

            sections = set([s.name for s in self.organisations + self.flat_roles])  # type: ignore #

            try:
                # Work in sets so there's no duplicates and we can do set based operations
                ad_target_state = set(ad.get_targets_state(
                    self.organisations,
                    self.flat_roles,
                    self.group_transforms,
                    self.role_transforms
                ))
            except TransformException as ex0:
                # already logged, just re-raise
                raise
            except LDAPExceptionError as ex1:
                # already logged, just re-raise
                raise

            try:
                db_present_state = set(db.get_group_role_state())

                creates   = ad_target_state.difference(db_present_state)
                unchanged = db_present_state.intersection(ad_target_state)
                removes   = db_present_state.difference(ad_target_state)

                log.info("{0:22}: {1:3} {2:5} to create, {3:3} {4:5} to remove, {5:3} {6:5} unchanged".format(
                    'Memberships',
                    len(creates),   pluraliser.plural('role', len(creates)),
                    len(removes),   pluraliser.plural('role', len(removes)),
                    len(unchanged), pluraliser.plural('role', len(unchanged))
                ))
            except Exception as ex:
                logging.exception(str(ex))
                raise

            try:
###### implicit db.begin_transaction() because autocommit=False
                # Create all the db groups and roles needed
                for section in sections:
                    log.debug('Creating root group roles for section {0}'.format(section))
                    db.create_if_not_exists_group_role(section)

                creates_parent_set = set( [ group_or_rolename[1] for group_or_rolename in creates] )
                for target in creates_parent_set:
                    prefix = target[:PGRType.len]
                    curr_name = target[PGRType.len:]
                    if prefix == PGRType.GROUP:
                        db.create_if_not_exists_group_role(curr_name)
                    else:
                        # Don't do this as CREATE ROLE IN GROUP just-in-case the role exists but isn't in the group
                        db.create_if_not_exists_role(curr_name)

                create_parent_members_dict: Dict[str, Set] = dict()
                for par, member in creates:
                    create_parent_members_dict.setdefault(par[PGRType.len:], set())
                    create_parent_members_dict[par[PGRType.len:]].add(member[PGRType.len:])
                for par, members in create_parent_members_dict.items():
                    log.debug('Assign memberships for group {0}'.format(par))
                    db.grant_group_to_roles(par, list(members))

                # Revoke all the extraneous memberships from groups and roles
                # This will kick out group and user roles in the same pass
                # A user might just have moved groups and not be completely removed from the tree
                removes_parent_dict: Dict[str, Set] = dict()
                for par, member in removes:
                    removes_parent_dict.setdefault(par[PGRType.len:], set())
                    removes_parent_dict[par[PGRType.len:]].add(member[PGRType.len:])
                for group, members in removes_parent_dict.items():
                    db.revoke_group_from_roles(group,list(members))


                # Tidy up / drop orphans
                managed_roles = db.get_managed_roles()
                # If a group or user role is commented as padnag managed and is no longer in any groups
                # then that user role can be "reassign owned by" in all dbs and then dropped.
                # Except the section roots, they're not members
                typed_sections = [PGRType.GROUP + s for s in sections]
                orphans = [(name[PGRType.len:], man, unman) for (name, man, unman) in managed_roles if
                           not (man or unman) and name not in typed_sections]
                orphan_roles_list = [name for (name, man, unman) in orphans]

                # NB revoke_all_priv_all_objs() does a COMMIT;
                db.revoke_all_priv_all_objs([db.current_catalog, ], orphan_roles_list)

                log.info('Orphaned roles to drop: {0:3}'.format(len(orphans)))

                sp_name = 'padnag_drop_role_savepoint'
                for role_name in orphan_roles_list:
                    try:
                        db.savepoint_start(sp_name)
                        db.drop_role_if_exists_quiet(role_name)
                        db.savepoint_release(sp_name)
                    except:
                        db.savepoint_rollback(sp_name)


                db.commit_transaction()
######  main db txn ended
            except Exception as ex:
                db.rollback_transaction()
                logging.exception(str(ex))
                raise

            try:
###### implicit db.begin_transaction() because autocommit=False

                # If a user role is commented as managed by padnag and appears in non padnag managed groups, warn and
                # remove the managed comment
                rejects = [(name, man, unman) for (name, man, unman) in managed_roles if (unman)]
                for name, man, unman in rejects:
                    role_name = name[PGRType.len:]
                    warnings.warn(
                        'Someone assigned role "{0}" to a group not managed by padnag. Abandoning role "{0}"'.format(
                            role_name))
                    db.unmanage_role(role_name)

                # If a group role is commented as padnag managed, but has no parent and no members,
                # then it can be reassigned owned by (in all catalogs) and dropped.
                # ?? This is covered by the drop_role_if_exists() loop ???


                managed_roles = db.get_managed_roles()
                orphans = [(name[PGRType.len:], man, unman) for (name, man, unman) in managed_roles if
                           not (man or unman) and name not in typed_sections]
                orphan_roles_list = [name for (name, man, unman) in orphans]

                other_catalogs = [d for d in db.list_catalogs() if d != db.current_catalog]
                db.rollback_transaction() # Because of autocommit = False on default connection
                if orphan_roles_list:
                    db.revoke_all_priv_all_objs(other_catalogs, orphan_roles_list)

                if orphan_roles_list:
                    for role_name in orphan_roles_list:
                        db.drop_role_if_exists(role_name)
                    db.commit_transaction()
###### other db txn end
            except Exception as ex:
                db.rollback_transaction()
                logging.exception(str(ex))
                raise

