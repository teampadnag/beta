from typing import Any, Dict, List, Tuple, Union
from ldap3 import Connection, SUBTREE, Server
from ldap3.core.exceptions import LDAPExceptionError, LDAPInvalidFilterError
from padnag.transformer import Transformer, Transform
from padnag.database import PGRType
from padnag.settings import SettingsOrganisation, SettingsFlatRole

import logging
import warnings
log = logging.getLogger(__name__)


class ADConnection(object):
    """
    A context manager for AD connections modeled after the DB version
    using "static" members ( i.e. on the Class ) for connection params
    http://stackoverflow.com/a/27568860/266387
    """

    conn_args = ()
    conn_kwargs: Dict[str, Union[str, int]] = {} # Union because port gets converted to an int

    def __init__(self) -> None:
        self.conn: Connection = None
        try:
            # Are we testing and going to use a mock AD
            self.conn = getattr(self.__class__, 'fake_connection')
            log.debug('Mocked AD Connection set up')
        except AttributeError as ignore:
            if not getattr(self.__class__, 'conn_args', False) and not getattr(self.__class__, 'conn_kwargs', False):
                # AD credentials could come from ~/.netrc file and they will be loaded from there.
                # but killing that behaviour for now
                raise AssertionError('AD connection parameters not set')
            try:
                args: List[Any] = getattr(self.__class__, 'conn_args', [])
                kwargs: Dict[str, Any] = getattr(self.__class__, 'conn_kwargs', {})
                self.conn = Connection(*args, **kwargs)
                log.debug('Real AD Connection set up')

            except Exception as ex:
                log.critical("AD connect failed: %s" % ex)
                raise

    def __enter__(self, read_server_info=False):
        try:
            self.conn.bind(read_server_info)
            return self.conn
        except Exception as ex:
            log.debug(ex.args[0])
            raise

    def __exit__(self, exception_type, exception_value, traceback) -> None:
        self.conn.unbind()


class ADLogic(object):
    """
    Stuff that happens against the AD
    """

    def __init__(self) -> None:
        self._ad_flat_membership: List[Tuple[str, str]] = []  # a list of (parent_role_or_group, child_role_or_group) tuples
        #
        self.transformer = Transformer()


    def search_ad(self, search_base: str, search_filter:str, search_scope: str, attributes: Union[List[str],str], paged_size: int=777) -> List[Dict[Any, Any]]:
        """
        :return: List of LDAP3 Entries
        """
        try:
            log.debug("Interrogating AD base {0} with filter {1}".format(search_base, search_filter))
            with ADConnection() as conn:
                entries = conn.extend.standard.paged_search(
                    search_base=search_base,
                    search_filter=search_filter,
                    search_scope=search_scope,
                    attributes=attributes,
                    paged_size=paged_size,
                    generator=False,
                )
            # https://mail.python.org/pipermail/python3-ldap/2014/000042.html
            return [entry for entry in entries if entry['type'] == 'searchResEntry']
        except LDAPInvalidFilterError as ex1:
            log.critical('Invalid AD filter: "{0}"'.format(search_filter))
            raise
        except LDAPExceptionError as ex2:
            log.exception(ex2.args[0])
            raise


    def get_targets_state(self, organisations: List[SettingsOrganisation], flat_roles: List[SettingsFlatRole],
                          group_transforms: List[Transform], role_transforms: List[Transform]) -> List[Tuple[str, str]]:
        """
        Get AD info into a list of( parent_role_or_group, child_role_or_group ), like the db query returns
        :return:
        """

        self._ad_flat_membership = []

        # Organisations
        for org in organisations:
            try:
                parent_group_name = PGRType.GROUP + org.name
                filter = org.filter_builder.format(org.filter)
                self._traverse_ad_group_flat(filter, org.name, group_transforms, role_transforms, org.base,
                                        None, parent_group_name, org.group_attribute, org.role_attribute)
            except Exception as ex:
                log.critical('A critical error occurred querying AD for [[Organisation]] {!s}'.format(org.name))
                raise

        # Flat_Roles
        for fr_section in flat_roles:
            try:
                parent_group_name = PGRType.GROUP + fr_section.name
                role_attribute = fr_section.role_attribute
                filter = fr_section.filter_builder.format(fr_section.filter)
                entries = self.search_ad(fr_section.base, filter, SUBTREE,
                                         ['objectClass', 'objectGUID', role_attribute])
                for entry in entries:
                    eatts = entry['attributes']
                    if 'group' not in eatts['objectClass']:
                        prefix = PGRType.ROLE
                        val = eatts[role_attribute]
                        if isinstance(val, list):
                            if len(val) > 1:
                                warnings.warn(
                                    'Unexpected multi-value list for AD role attribute {0}. Going with the first value found'.format(
                                        role_attribute))
                            val = val[0]
                        transformed = self.transformer.transform_ad_value(role_transforms, fr_section.name, val)
                        self._ad_flat_membership.append( (parent_group_name, prefix + transformed) )
            except Exception as ex:
                log.critical('A critical error occurred querying AD for [[Flat_Role]] {!s}'.format(fr_section.name))
                raise

        return self._ad_flat_membership


    def _traverse_ad_group_flat(self, recursing: str, organisation: str, group_transforms: List[Transform],
                                role_transforms: List[Transform], search_base: str, group_dn: str,
                           parent_group_name: str, group_attribute: str, role_attribute: str) -> None:
        """
        :param recursing:
        :param group_dn: only if not recursing. Otherwise just pass None
        :return: The group and role names have been transformed from their AD to their db equivalents and
                 prefixed with PGRType
        """
        if not (len(recursing) > 0):
            search_filter = "(&(|(objectClass=group)(objectClass=user))(memberOf={0}))".format(group_dn)
        else:
            search_filter = recursing

        entries = self.search_ad(search_base, search_filter, SUBTREE,
                                 ['objectClass', 'objectGUID', group_attribute, role_attribute])
        for entry in entries:
            eatts = entry['attributes']
            if 'group' in eatts['objectClass']:
                prefix = PGRType.GROUP
                val = eatts[group_attribute]
                if isinstance(val, list):
                    if len(val) > 1:
                        warnings.warn('Unexpected multi-value list for AD group attribute {0}. Going with the first value found'.format(group_attribute))
                    else:
                        val = val[0]
                transformed = self.transformer.transform_ad_value(group_transforms, organisation, val)
            else:
                prefix = PGRType.ROLE
                val = eatts[role_attribute]
                if isinstance(val, list):
                    if len(val) > 1:
                        warnings.warn('Unexpected multi-value list for AD role attribute {0}. Going with the first value found'.format(role_attribute))
                    else:
                        val = val[0]
                transformed = self.transformer.transform_ad_value(role_transforms, organisation, val)

            self._ad_flat_membership.append( (parent_group_name, prefix + transformed) )

            if 'group' in eatts['objectClass']:
                self._traverse_ad_group_flat("", organisation, group_transforms, role_transforms, search_base,
                                        entry['dn'],
                                        prefix + transformed, group_attribute, role_attribute
                                        )