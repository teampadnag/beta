from typing import Dict,List,Match,Tuple, Union
from enum import Enum, unique
import re
import logging
import warnings
log = logging.getLogger(__name__)


@unique
class GlobalLabel(Enum):
    GROUP = '__GROUP_TRANSFORMS__'
    ROLE = '__ROLE_TRANSFORMS__'


class TransformException(Exception):
    pass

class Transform(object):
    """
    Now a class so can have __str__ and __eq__
    """

    def __init__(self, group_role: Union[str, GlobalLabel], pattern: str, repl: str, ad_attribute: str, kount: int, flags: int) -> None:
        self.key = group_role
        self.pattern = pattern
        self.repl = repl
        self.ad_attribute = ad_attribute
        self.count = kount
        self.flags = flags

    def __eq__(self, other) -> bool:
        # needed for unittest.assertEqual
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return str(self.__dict__)

    def __str__(self) -> str:
        return "re.sub(\'{0}\', \'{1}\', ...)".format( self.pattern, self.repl )


class Transformer(object):
    """
    Apply user defined regexes to the stuff coming back form the AD
    """

    def __init__(self):
        self.parser_regex = re.compile(r'''
                ^\s*                                                 #  start anchor and leading whitespace
                re\.sub\s?                                           #  re.sub plus whitespace
                \(\s*                                                #  open paren & whitespace
                [r]?(?P<quote1>['"]+)(?P<pattern>.*?)(?P=quote1)
                \s*[,]\s*                                            #  whitespace comma
                [r]?(?P<quote2>['"]*)(?P<repl>.*?)(?P=quote2)        #  repl. quotes options because of lower & upper functions
                \s*[,]\s*                                            #  whitespace comma
                (?P<ad_attribute>\w+)                                #  AD attribute
                (\s*[,]\s*)?                                         #  whitespace comma
                ((count\s*[=]\s*)?(?P<kw_count>\d+)[,]?)?            #  => group "kw_count"
                (\s*[,]\s*)?                                         #  whitespace comma
                ((flags\s*[=]\s*)?(?P<kw_flags>[\w\.\|\s]+)[,]?)?    #  => group "kw_flags"
                \s?[)]                                               #  whitespace close paren
                (?P<comment>\s?[#].*)?                               #  whitespace and comment
                (?P<leftovers>.*?)
                ''', re.VERBOSE)
        # pprint(ex.fullmatch(str).groupdict())

    def parse_transforms(self, raw_group_transforms: Dict[Union[str, GlobalLabel], List[str]],
                                raw_role_transforms: Dict[Union[str, GlobalLabel], List[str]]) -> Tuple[List[Transform],List[Transform]]:
        """
        Make sure that the transforms are valid python re.sub syntax
        If not, log a warning and raise an InvalidConfigException
        :return: group_transforms, role_transforms
        """

        group_transforms: List[Transform] = []
        role_transforms: List[Transform] = []

        error_found = False
        for idx, transdict in enumerate([raw_group_transforms, raw_role_transforms]):
            for group_role_key, transforms in transdict.items():
                # may or not be a list
                if isinstance(transforms, str):
                    transforms = [transforms, ]
                for transform in transforms:
                    match = self.parser_regex.fullmatch(transform)
                    if match:
                        pt = Transform(group_role_key,
                                       match.group('pattern').strip(),
                                       match.group('repl').strip(),
                                       match.group('ad_attribute').strip(),
                                       int(match.group('kw_count')),
                                       int(match.group('kw_flags'))
                                       )
                        if idx == 0:
                            group_transforms.append(pt)
                        else:
                            role_transforms.append(pt)

                    else:
                        warnings.warn("Couldn't parse transform for {1}\nTransform was: \"{0}\"".format(str(transform), group_role_key))
                        error_found = True
        try:
            return group_transforms, role_transforms
        finally:
            if error_found:
                raise TransformException('''Couldn't parse one or more transforms''')


    def transform_ad_value(self, parsed_transforms: List[Transform], transform_key: str, ad_value: str) -> str:
        """
        Run the sAMAccountName through the appropriate transforms as defined in the config file
        :param transform_key: Which organisations transforms to apply
        :param ad_value: what to transform
        :return: the transformed sAMAccountName to be used as the databse role name
        """

        def lcase_func(pat: Match) -> str:
            return pat.group(0).lower()

        def ucase_func(pat: Match) -> str:
            return pat.group(0).upper()

        ret = ad_value

        for transform in parsed_transforms:
            if transform.key in (transform_key, GlobalLabel.GROUP, GlobalLabel.ROLE):
                try:
                    if transform.repl == 'lower':
                        ret = re.sub(transform.pattern, lcase_func, ret, transform.count, transform.flags)
                    elif transform.repl == 'upper':
                        ret = re.sub(transform.pattern, ucase_func, ret, transform.count, transform.flags)
                    else:
                        ret = re.sub(transform.pattern, transform.repl, ret, transform.count, transform.flags)
                except Exception as ex:
                    log.exception('Exception occurred while transforming {0} for section {1} with regex {2}'.format(ad_value, transform.key, transform.pattern.encode('unicode_escape')))
                    raise TransformException('Exception occurred while transforming regex for {0} for section {1}'.format(ad_value, transform.key))

                if ad_value != ret:
                    log.debug('Transform for {0} : changed {1} to {2}'.format(transform.key, ad_value, ret))

        return ret
