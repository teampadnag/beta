from typing import Any, cast, Dict, Generator, List, Sequence, Tuple, Union
import pg8000
from collections import namedtuple

import logging
log = logging.getLogger(__name__)

PostgresqlPrivileges = namedtuple('PostgresqlPrivileges',
                                  ['rolsuper',
                                   'rolinherit',
                                   'rolcreaterole',
                                   'rolcreatedb',
                                   'rolcanlogin',
                                   'rolreplication',
                                   'rolconnlimit',
                                   'rolpassword',
                                   'rolvaliduntil',
                                   'rolbypassrls',
                                   'rolconfig'
                                   ]
                                  )


class PGRType(object):
    """
    (P)ost(G)regsql(R)ole(T)ype
    NB. lengths must match
    """
    GROUP = ']_G_[__'
    ROLE  = ']_R_[__'
    assert (len(GROUP) == len(ROLE))
    # Oops should have named this better
    len = len(GROUP)



class DBConn(object):
    """
    A context manager for database connections
    using "static" members ( i.e. on the Class ) for connection params
    http://stackoverflow.com/a/27568860/266387
    """
    conn_args = ()
    conn_kwargs: Dict[str, Union[str, int]] = {} # Union because port is an int

    def __init__(self, override_db: str=None) -> None:
        if not getattr(self.__class__, 'conn_args', False) and not getattr(self.__class__ ,'conn_kwargs', False):
            log.error('Database connection parameters not set')
            raise AssertionError('Database connection parameters not set')
        try:
            args = getattr(self.__class__, 'conn_args', [])
            kwargs = getattr(self.__class__, 'conn_kwargs', {})
            if override_db:
                kwargs['database'] = override_db

            self.conn = pg8000.connect(*args, **kwargs)
            # Following the DB-API specification, autocommit is off by default.
            # It can be turned on by setting this boolean pg8000-specific autocommit property to True.
            self.conn.autocommit = False

            cursor = self.conn.cursor()
            cursor.execute("SELECT current_database(), current_user;")
            row = cursor.fetchone()
            self.conn.current_catalog = row[0]
            self.conn.current_user = row[1]



        except pg8000.InterfaceError as ex:
            log.debug("Exception related to the database connection. e.g. Connection refused or mis-configured SSL.")
            raise ex

        except pg8000.InternalError as ex:
            # This is currently only raised when unexpected state occurs in the pg8000 interface itself,
            # and is typically the result of a interface bug.
            log.debug("Database API encountered an internal error.")
            raise ex

        except pg8000.OperationalError as ex:
            #  This exception is currently never raised by pg8000.
            #  Not necessarily under the control of the programmer
            log.debug("Errors related to the database’s operation.")
            raise ex

        except pg8000.ProgrammingError as ex:
            # For example, this exception is raised if more parameter fields are in a query string than there are
            # available parameters.
            log.debug("Programming error.")
            raise ex

        except pg8000.IntegrityError as ex:
            # . This exception is not currently raised by pg8000
            log.debug("Exception with the relational integrity of the database being affected.")
            raise ex

        except pg8000.DataError as ex:
            # . This exception is not currently raised by pg8000
            log.debug("Problems with the processed data.")
            raise ex

        except pg8000.NotSupportedError as ex:
            log.debug("Method or database API was used which is not supported by the database.")
            raise ex
            
        except pg8000.DatabaseError as ex:
            # This exception is currently never raised by pg8000.
            log.debug(" exception raised for errors that are related to the database.")
            raise ex
    
        except pg8000.Error as ex:
            log.debug("Other database exception.")
            raise ex
            
        except Exception as ex:
            log.debug("Unrecognised exception from database API")
            raise ex


    def __enter__(self):
        return self.conn

    def __exit__(self, exception_type, exception_value, traceback):
        self.conn.close()



class DBLogic(object):
    """
    sql strings are laid out with specific spacing for neat verbose logging.
    sql string must be ; terminated for easy grep + copy paste
    """

    MANAGED_COMMENT_FLAG = 'Managed by padnag'

    @property
    def current_catalog(self):
        return self.conn.current_catalog

    @property
    def current_user(self):
        return self.conn.current_user


    def __init__(self, conn: pg8000.core.Connection, test_mode: bool = False, verbose: bool = False) -> None:
        self.conn = conn
        self.test_mode = test_mode
        self.verbose = verbose
        self.sql_log = logging.getLogger(__name__ + '.sql')


    def savepoint_start(self, name: str) -> None:
        self.conn.autocommit = False
        self._sqlFetchNone('''
        SAVEPOINT {0};'''.format(name), test_mode=False)

    def savepoint_release(self, name: str) -> None:
        self.conn.autocommit = False
        self._sqlFetchNone('''
        RELEASE SAVEPOINT {0};'''.format(name), test_mode=False)

    def savepoint_rollback(self, name: str) -> None:
        self.conn.autocommit = False
        self._sqlFetchNone('''
        ROLLBACK TO SAVEPOINT {0};'''.format(name), test_mode=False)


    def commit_transaction(self) -> None:
        self.conn.autocommit = False
        self._sqlFetchNone('''
        COMMIT TRANSACTION;''', test_mode=False)

    def rollback_transaction(self) -> None:
        self.conn.autocommit = False
        self._sqlFetchNone('''
        ROLLBACK TRANSACTION;''', test_mode=False)


    def check_alive(self) -> bool:
        """
        Check if / Keep the connection alive
        """
        sql = """
        SELECT NOW() AS alive;"""
        result = self._sql_fetch_one(sql, test_mode=False)
        if result:
            return True
        else:
            return False

    def check_program_privileges(self) -> bool:
        """
        Make sure the role the application is using has enough privileges to create, reassign and drop roles.
        rolcreate is not enough
        """
        sql = """
        SELECT rolsuper FROM pg_roles WHERE rolname = current_user LIMIT 1;"""
        result = self._sql_fetch_one(sql, test_mode=False)
        return cast(bool, result) and cast(bool, result[0])


    def list_catalogs(self) -> List[str]:
        """
        :return: list
        """
        # Don't exclude postgres db, there shouldn't but could be objects in there
        sql = """SELECT datname FROM pg_database WHERE datistemplate = False; """
        rows = self._sql_fetch_all(sql, False)
        return [rr[0] for rr in rows]


    def get_group_role_state(self) -> List[Tuple[str, ...]]:
        """
        Returns the parent - child relationships for all group roles and group member roles.
        :param group_role:
        :return: list of tuples
        """
        results: List[Tuple[str, ...]] = []
        sql = """        -- get_group_role_state
        SELECT
        CASE WHEN parnt.rolcanlogin THEN '{0}' || parnt.rolname ELSE '{1}' || parnt.rolname END AS parent,
        CASE WHEN membr.rolcanlogin THEN '{0}' || membr.rolname ELSE '{1}' || membr.rolname END AS member
        FROM pg_auth_members AS m
        INNER JOIN pg_roles AS parnt
            ON m.roleid = parnt.oid
        INNER JOIN pg_roles AS membr
            ON m.member = membr.oid
        ORDER BY 1,2; """.format(PGRType.ROLE, PGRType.GROUP)
        rows = self._sql_fetch_all(sql, False)

        results = [(rr[0], rr[1]) for rr in rows]

        return results


    def get_managed_roles(self, managed_comment_flag: str=None) -> List[Tuple[str, str, str]]:
        """
        :param comment_flag:
        :return: list of tuples [ (role_name, any_managed_membership, any_unmanaged_membership)]

        it's a list of tuples for consistency with other methods

        """
        if managed_comment_flag is None:
            managed_comment_flag = self.MANAGED_COMMENT_FLAG
        results: List[Tuple[str, str, str]] = []
        sql = """    --get_padnag_managed_roles
        SELECT  CASE WHEN rr.rolcanlogin THEN '{0}' || rr.rolname ELSE '{1}' || rr.rolname END AS role_name,
        True = ANY(ARRAY(
        SELECT coalesce(pg_catalog.shobj_description(m.roleid, 'pg_authid'),'***') ~ '{2}'
        FROM pg_catalog.pg_auth_members m WHERE (rr.oid = m.member)
        UNION ALL
        SELECT False )) AS any_managed_membership,
        True = ANY(ARRAY(
        SELECT coalesce(pg_catalog.shobj_description(m.roleid, 'pg_authid'),'***') !~ '{2}'
        FROM pg_catalog.pg_auth_members m WHERE (rr.oid = m.member)
        UNION ALL
        SELECT False )) AS any_unmanaged_membership
        FROM pg_catalog.pg_roles rr
        WHERE
        pg_catalog.shobj_description(rr.oid, 'pg_authid') ~ '{2}'
        ORDER BY 1,2,3; """.format(PGRType.ROLE, PGRType.GROUP, self.MANAGED_COMMENT_FLAG)
        rows = self._sql_fetch_all(sql, False)
        return [(rr[0], rr[1], rr[2]) for rr in rows]


    def create_if_not_exists_group_role(self, group_role: str) -> bool:
        """
        These are the group roles that the DBA should give privileges to , and that AD group members will be created in
        :param group_role:
        :return:
        ToDo: Explain or remove why the is different style from create_if_not_exists_role()
        """
        check_role_sql = """
        SELECT 'exists' FROM pg_roles WHERE rolname = %s LIMIT 1"""
        row = self._sql_fetch_one(check_role_sql, False, (group_role,))
        if not row:
            self.create_group_role(group_role)
        return True

    def create_if_not_exists_role(self, rolename: str) -> bool:
        """
        These are the AD username matching roles
        :param rolename:
        :return:
        """
        sql = """
        DO
        $body$BEGIN
           IF NOT EXISTS (SELECT 1 FROM   pg_catalog.pg_user WHERE  usename = '{0}') THEN CREATE ROLE "{0}" WITH LOGIN; COMMENT ON ROLE "{0}" IS '{1}'; END IF;
        END$body$;""".format(rolename, self.MANAGED_COMMENT_FLAG)
        if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
            self.sql_log.info(sql)
        self._sql(sql, fetch=None, test_mode=self.test_mode)
        return True

    def create_group_role(self, rolename: str) -> None:
        """
        The SQL query to create a group role
        # ToDo: Handle invalid role names gracefully
        """

        sql1 = """
        CREATE ROLE "{0}" WITH NOLOGIN; """.format(rolename)
        sql2 = """
        COMMENT ON ROLE "{0}" IS '{1}'; """.format(rolename, self.MANAGED_COMMENT_FLAG)
        if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
            self.sql_log.info(sql1)
            self.sql_log.info(sql2)
        self._sql(sql1, fetch=None, test_mode=self.test_mode)
        self._sql(sql2, fetch=None, test_mode=self.test_mode)


    def grant_group_to_roles(self, group_role: str, roles_list: List[str]) -> None:
        """
        The SQL query to grant a group to roles
        ignoring SQL injection. pg8000 escaping is problematic. If Bobby Tables is in the AD there's bigger problems.
        Chunking because AD group might have hundreds or thousands of members and become illegible,unparseable etc.
        """
        for some in self._chunks(roles_list, 21):
            sql = """
        GRANT "{0}" TO {1};""".format(group_role, ', '.join('"' + r +'"' for r in some))
            if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
                self.sql_log.info(sql)
            self._sql(sql, fetch=None, test_mode=self.test_mode)


    def revoke_group_from_roles(self, group_role: str, roles_list: List[str]) -> None:
        """
        The SQL query to revoke a group from the roles
        ignoring SQL injection. pg8000 escaping is problematic. If Bobby Tables is in the AD there's bigger problems.
        Chunking because AD group might have hundreds or thousands of members and become illegible,unparseable etc.
        """
        for some in self._chunks(roles_list, 21):
            sql = """
        REVOKE "{0}" FROM {1};""".format(group_role, ', '.join('"' + r +'"' for r in some))
            if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
                self.sql_log.info(sql)
            self._sql(sql, fetch=None, test_mode=self.test_mode)


    def unmanage_role(self, role_name: str) -> None:
        """
        DBA has messed with role so don't drop it, just remove manage flag
        :return:
        """
        sql1 = """
        SELECT REPLACE(pg_catalog.shobj_description(rr.oid, 'pg_authid'),'{1}','')
        FROM pg_roles as rr WHERE rolname = '{0}'; """.format(role_name, self.MANAGED_COMMENT_FLAG)
        result = self._sql_fetch_one(sql1 ,self.test_mode)
        if result:
            comment = result[0]
        else:
            comment = ''
        sql2= """
        COMMENT ON ROLE "{0}" IS '{1}' ;""".format(role_name, comment)
        return self._sqlFetchNone(sql2, self.test_mode)


    def drop_role_if_exists(self, role_name: str) -> bool:
        """
        :param rolename:
        :return:
        """
        sql = """
        DROP ROLE IF EXISTS "{0}"; """.format(role_name)
        if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
            self.sql_log.info(sql)
        self._sqlFetchNone(sql, self.test_mode)
        return True


    def drop_role_if_exists_quiet(self, role_name: str) -> bool:
        """
        Try drop a role, not log error like "cannot be dropped because some objects depend on it"
        :param rolename:
        :return:
        """
        sql = """
        DROP ROLE IF EXISTS "{0}"; """.format(role_name)
        if self.verbose and log.getEffectiveLevel() > logging.DEBUG:
            self.sql_log.info(sql)
        try:
            cursor = self.conn.cursor()
            # cursor.execute("SET CLIENT_ENCODING TO 'UTF8'") # Breaks ROLLBACK
            self.sql_log.debug(sql, [])
            cursor.execute(sql)
        except pg8000.core.ProgrammingError as ignored:
            # Don't log it, we're in quiet mode
            raise
        except Exception as ex:
            logging.exception(str(ex))
            raise
        finally:
            cursor.close()


        self._sqlFetchNone(sql, self.test_mode)
        return True

    def revoke_all_priv_all_objs(self, catalogs: List[str], role_names: List[str]) -> None:
        """
        Uses a second connection
        :param catalogs: 
        """
        args = ()
        sql_commands = []
        revoke_function_sql = ""
        schemas_sql = []
        db_sql = []
        reassign_sql = []
        drop_function_sql = ""

        if role_names:
            for some in self._chunks(role_names, 21):
                # < pg9.5 doesn't support current_user syntax
                reassign_sql.append("""
        REASSIGN OWNED BY {0} TO "{1}"; """.format(', '.join('"' + r +'"' for r in some), self.current_user) )

            revoke_function_sql = """
        CREATE OR REPLACE FUNCTION padnag_revoke_all_privileges(role_name VARCHAR)
        RETURNS void
        AS $$
        DECLARE cursor_row RECORD;
        BEGIN
            FOR cursor_row IN
                SELECT
                 'REVOKE ALL PRIVILEGES ON ALL TABLES    IN SCHEMA ' || QUOTE_IDENT(n.nspname) || ' FROM ' || QUOTE_IDENT( $1 ) AS tables_sql
                ,'REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA ' || QUOTE_IDENT(n.nspname) || ' FROM ' || QUOTE_IDENT( $1 ) AS sequences_sql
                ,'REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA ' || QUOTE_IDENT(n.nspname) || ' FROM ' || QUOTE_IDENT( $1 ) AS functions_sql
                ,'REVOKE ALL PRIVILEGES ON SCHEMA '                  || QUOTE_IDENT(n.nspname) || ' FROM ' || QUOTE_IDENT( $1 ) AS schema_sql
                FROM pg_catalog.pg_namespace n
                WHERE n.nspname !~ '^pg_' AND n.nspname <> 'information_schema'
            LOOP
                EXECUTE cursor_row.tables_sql    USING role_name;
                EXECUTE cursor_row.sequences_sql USING role_name;
                EXECUTE cursor_row.functions_sql USING role_name;
                EXECUTE cursor_row.schema_sql    USING role_name;
            END LOOP;
        END$$
        LANGUAGE plpgsql VOLATILE;
        """

            for rn in role_names:
                schemas_sql.append("""
        SELECT padnag_revoke_all_privileges('{0}');""".format(rn) )
                for db in catalogs:
                    db_sql.append("""
        REVOKE ALL PRIVILEGES ON DATABASE {1} FROM "{0}"; """.format(rn, db) )

            # Done with role_names loop
            drop_function_sql = """
        DROP FUNCTION IF EXISTS padnag_revoke_all_privileges(role_name VARCHAR); """




            sql_commands.extend(reassign_sql)
            sql_commands.append(revoke_function_sql)
            sql_commands.extend(schemas_sql)
            sql_commands.extend(db_sql)
            sql_commands.append(drop_function_sql)

        commit_sql = """
        COMMIT;
            """
        sql_commands.append(commit_sql)
        self._sql_multi_catalogs(catalogs, sql_commands, self.test_mode, *args)


    def _chunks(self, lst: List[Any], chunk_size: int) -> Generator:
        """
        Yield successive chunk_size-sized chunks from lst.
        Thanks: http://stackoverflow.com/a/312464/266387
        """
        for i in range(0, len(lst), chunk_size):
            yield lst[i:i + chunk_size]


    def _sql(self, sql: str, fetch: int, test_mode: bool, *args) -> Union[None,Sequence[Any],Sequence[Sequence[Any]]]:
        """
        :param sql:
        :param fetch:
        :param args:
        :return:
        """
        try:
            cursor = self.conn.cursor()
            # cursor.execute("SET CLIENT_ENCODING TO 'UTF8'") # Breaks ROLLBACK
            self.sql_log.debug(sql, *args)
            if test_mode:
                return None
            else:
                cursor.execute(sql, *args)
                if not fetch:
                    return None
                elif fetch == 1:
                    return cursor.fetchone()
                else:
                    return cursor.fetchall()

        except Exception as ex:
            logging.exception(str(ex))
            raise
        finally:
            cursor.close()


    def _sqlFetchNone(self, sql: str, test_mode: bool, *args) -> None:
        """
        Convenience method. Autocommit=False
        """
        self._sql(sql, None, test_mode, *args)

    def _sql_fetch_one(self, sql: str, test_mode: bool, *args) -> Sequence[Any]:
        """
        Convenience method. Autocommit=False
        """
        return self._sql(sql, 1, test_mode, *args)

    def _sql_fetch_all(self, sql: str, test_mode: bool, *args) -> Sequence[Sequence[Any]]:
        """
        Convenience method. Autocommit=False
        """
        return self._sql(sql, -1, test_mode, *args)

    def _sql_multi_catalogs(self, catalogs: List[str], sql_commands: List[str], test_mode: bool, *args) -> None:
        """        
        NB
        Main DBConn connection doesn't autocommit.
        Other db sql is committed straight away.
        No fetching allowed.  
        :return:
        """

        if self.current_catalog in catalogs:
            for sql in sql_commands:
                self._sqlFetchNone(sql, test_mode, *args)

        for db in [cc for cc in catalogs if cc != self.current_catalog]:
            try:
                with DBConn(override_db=db) as conn2:
                    conn2.autocommit = True
                    cursor2 = conn2.cursor()
                    # cursor2.execute("SET CLIENT_ENCODING TO 'UTF8'")
                    if self.verbose:
                        self.sql_log.info(""" -- USE {0};""".format(db))
                    else:
                        self.sql_log.debug(""" -- USE {0};""".format(db))
                    for sql in sql_commands:
                        if self.verbose:
                            self.sql_log.info(sql, *args)
                        else:
                            self.sql_log.debug(sql, *args)
                        if test_mode:
                            pass
                        else:
                            cursor2.execute(sql, *args)

            except Exception as err:
                raise err
            finally:
                cursor2.close()
