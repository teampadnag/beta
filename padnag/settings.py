from typing import Any, Dict, List, Tuple, Union
import logging
import os
import warnings
import pytoml as toml

from padnag.transformer import Transformer, Transform, GlobalLabel, TransformException

log = logging.getLogger(__name__)


class InvalidConfigException(Exception):
    pass


class SettingsFlatRole(object):
    def __init__(self, name: str, base: str, filter: str,
                 filter_builder: str = '(&(objectClass=user)({0}))',  # hidden setting
                 role_attribute: str = 'sAMAccountName',
                 role_transforms: List[Transform] = []) -> None:
        #
        self.name = name
        self.base = base
        self.filter = filter
        self.filter_builder = filter_builder
        self.role_attribute = role_attribute
        self.role_transforms = role_transforms


class SettingsOrganisation(SettingsFlatRole):
    def __init__(self, name: str, base: str, filter: str,
                 filter_builder: str = '(&(|(objectClass=group)(objectClass=user))({0}))',  # hidden setting
                 role_attribute: str = 'sAMAccountName',
                 role_transforms: List[Transform] = [],
                 group_attribute: str = 'name',
                 group_transforms: List[Transform] = []) -> None:
        #
        super(SettingsOrganisation, self).__init__(name, base, filter, filter_builder, role_attribute, role_transforms)
        self.group_attribute  = group_attribute
        self.group_transforms = group_transforms



class Settings(object):
    """
    Settings & general config handler
    """

    def __init__(self) -> None:
        self.transformer = Transformer()
        self.config: Dict[Any, Any]            = None
        self.ini_general: Dict[str, Any]       = None
        self.ini_ad: Dict[str, Union[str,int]] = None
        self.ini_db: Dict[str, Union[str,int]] = None
        self.ini_organisations: List[SettingsOrganisation] = None
        self.ini_flat_roles:    List[SettingsFlatRole]     = None
        self.ini_group_transforms: List[Transform] = None
        self.ini_role_transforms: List[Transform]  = None


    def load_ini_file(self, config_file: str, overides: Dict[Any, Any]={}) -> bool:
        if not os.path.isfile(config_file):
            log.debug('config file %s not found' % config_file)
            raise FileNotFoundError('config file %s not found' % config_file)
        with open(config_file) as tomlfile:
            cfg = tomlfile.read()
            return self.load_ini_string(cfg, overides)


    def load_ini_string(self, config_string: str, overides: Dict[Any, Any]={}) -> bool:
        try:
            self.config = toml.loads(config_string)
            return self._load_config(overides)

        except toml.core.TomlError as ex:
            msg = 'Config file is not valid TOML. Check {0} around {1}, {2}'.format(ex.filename, ex.line, ex.col)
            log.debug(msg)
            raise InvalidConfigException(msg)

        except TransformException as ex:
            # The warning has already been logged, just re-raise
            raise

        except InvalidConfigException as ex:
            log.debug(str(ex))
            raise

        except ValueError as ex:
            # dud int(port) conversion
            log.debug('Error converting database port setting to an integer')
            raise

        except Exception as ex:
            log.debug(str(ex))
            raise


    def _merge_config_dicts(self, dict_1: Dict[Any, Any], dict_2: Dict[Any, Any]) -> Dict[Any, Any]:
        """
        Thanks: https://github.com/docopt/docopt/blob/master/examples/config_file_example.py
        Merge two dictionaries.
        Values that evaluate to true take priority over falsy values.
        `dict_1` takes priority over `dict_2`.
        """
        return dict((str(key), dict_1.get(key) or dict_2.get(key)) for key in set(dict_2) | set(dict_1))


    def _parseFlatRoles(self, config: Dict[str,Any]) -> List[SettingsFlatRole]:
        ret: List[SettingsFlatRole] = []
        if not isinstance(config, list):
            tmp = [config, ]
        else:
            tmp = config
        return [SettingsFlatRole(**section) for section in tmp]


    def _parseOrganisations(self, config: Dict[str,Any]) -> List[SettingsOrganisation]:
        ret: List[SettingsOrganisation] = []
        if not isinstance(config, list):
            tmp = [config, ]
        else:
            tmp = config
        return [SettingsOrganisation(**section) for section in tmp]


    def _load_config(self, overrides :Dict[str,Union[Dict[str, Any], List[Any]]] = {}) -> bool:
        self.config = self._merge_config_dicts(overrides, self.config)
            # some defaults
        self.config.setdefault('General', {})
        self.config.setdefault('Active_Directory', {})
        self.config.setdefault('Organisation', [])
        self.config.setdefault('Flat_Role', [])

        self.config['General'] = self._merge_config_dicts(self.config['General'],{
                'verbose': False,
                'force': False,
                'test': False,
                'poll': 0,
            })
        self.config['Active_Directory'] = self._merge_config_dicts(self.config['Active_Directory'],{
                'raise_exceptions': True,
                'auto_bind': True,
                'read_only': True,
            })

        self._check_required(self.config)
        self._check_transforms_shape(self.config)
        self._check_superflous(self.config)

        self.ini_organisations = self._parseOrganisations(self.config['Organisation'])
        self.ini_flat_roles = self._parseFlatRoles(self.config['Flat_Role'])

        # Dup keys are not allowed in these sections
        self.ini_general = self.config['General']
        self.ini_ad      = self.config['Active_Directory']
        self.ini_db      = self.config['Database']

        # type change JIC
        self.ini_db['port'] = int(self.ini_db.get('port', 5432))

        # last in case of ParseExceptions and --force
        raw_group_transforms, raw_role_transforms = self._extract_raw_transforms(self.config)
        self.ini_group_transforms, self.ini_role_transforms = self.transformer.parse_transforms(raw_group_transforms, raw_role_transforms)

        return True


    def _check_required(self, config:Dict[str,Any]) -> bool:
        """
        Raises exceptions if required config is not present
        :param config:
        :return:
        """
        if not (config['Organisation'] or config['Flat_Role']):
            raise InvalidConfigException('Config must provide at least one [[Organisation]] or [[Flat_Role]] section')
        if not (config['Active_Directory']):
            raise InvalidConfigException('Active_Directory section not found in config')
        if not (config['Database']):
            raise InvalidConfigException('Database section not found in config')

        if config['Organisation']:
            for sect in config['Organisation']:
                for attr in ['name','base','role_attribute', 'group_attribute', ]:
                    if not sect.get(attr):
                        raise InvalidConfigException('Config {1} section missing required {0} attribute'.format(attr, '[[Organisation]]'))


        if config['Flat_Role']:
            for sect in config['Flat_Role']:
                for attr in ['name','base','role_attribute', ]:
                    if not sect.get(attr):
                        raise InvalidConfigException('Config {1} section missing required {0} attribute'.format(attr, '[[Flat_Role]]'))

        return True


    def _check_transforms_shape(self, config:Dict[str, Any]) -> bool:
        """
        Raises exceptions if group_transforms or role_transforms are not a list
        :param config:
        :return:
        """

        if config['Organisation']:
            for sect in config['Organisation']:
                if sect.get('group_transforms'):
                    if not isinstance(sect.get('group_transforms'), list):
                        warnings.warn('Config group_transforms must be a list, e.g. group_transforms = [\'...\',]  in [[Organisation]] {0}'.format(sect['name']))
                        sect['group_transforms'] = [sect['group_transforms'], ]  # Try anyway
                if sect.get('role_transforms'):
                    if not isinstance(sect.get('role_transforms'), list):
                        warnings.warn('Config role_transforms must be a list, e.g. role_transforms = [\'...\',]  in [[Organisation]] {0}'.format(sect['name']))
                        sect['role_transforms'] = [sect['role_transforms'], ]


        if config['Flat_Role']:
            for sect in config['Flat_Role']:
                if sect.get('role_transforms'):
                    if not isinstance(sect.get('role_transforms'), list):
                        warnings.warn('Config role_transforms must be a list, like role_transforms = [\'...\',]  in [[Flat_Role]] {0}'.format(sect['name']))
                        sect['role_transforms'] = [sect['role_transforms'], ]

        return True


    def _check_superflous(self, config: Dict[str, Any]) -> None:
        """
        Unrecognised settings are probably typos. Throw a warning
        :return:
        """
        for key, val in config.items():
            if key not in ('General', 'Active_Directory', 'Database', 'Organisation', 'Flat_Role', 'Global_Transforms'):
                warnings.warn('Unrecognised section "{0}" in config. Config is case sensitive'.format(key))


    def _extract_raw_transforms(self, config: Dict[str, Any]) -> Tuple[Dict[Union[str,GlobalLabel],List[str]],
                                                                       Dict[Union[str,GlobalLabel],List[str]]]:
        raw_group_transforms: Dict[Union[str,GlobalLabel],List[str]] = {}
        raw_role_transforms:  Dict[Union[str,GlobalLabel],List[str]] = {}

        for tr in config.get('Organisation', {}):
            raw_group_transforms.setdefault(tr['name'], [])
            raw_role_transforms.setdefault(tr['name'], [])
            for gt in tr.get('group_transforms', []):
                raw_group_transforms[tr['name']].append(gt)
            for rt in tr.get('role_transforms', []):
                raw_role_transforms[tr['name']].append(rt)

        for tr in config.get('Flat_Role', {}):
            raw_group_transforms.setdefault(tr['name'], [])
            raw_role_transforms.setdefault(tr['name'], [])
            for rt in tr.get('role_transforms', []):
                raw_role_transforms[tr['name']].append(rt)

        tr = config.get('Global_Transforms', {})
        raw_group_transforms.setdefault(GlobalLabel.GROUP, [])
        raw_role_transforms.setdefault(GlobalLabel.ROLE, [])
        for gt in tr.get('group_transforms', []):
            raw_group_transforms[GlobalLabel.GROUP].append(gt)
        for rt in tr.get('role_transforms', []):
            raw_role_transforms[GlobalLabel.ROLE].append(rt)

        return raw_group_transforms, raw_role_transforms
