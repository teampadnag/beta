© gregory nicol 2017.

padnag is licensed under the Business Source License (BSL). After the earlier of the license change date or four years from release, the source code becomes available free of charge under the Open Source License GNU GPL 2.0.

The BSL is designed to provide a mutually beneficial balance between the user benefits of true Open Source software that is free of cost and provides open access to all of the product code for modification, distribution, etc., and the sustainability needs of software developers to continue delivering product innovation and maintenance.

The BSL is used in internationally successful enterprise class products like MariaDB MaxScale™.

For more information on the use of the Business Source License for MariaDB products, please visit the MariaDB Business Source License FAQ. For more information on the use of the Business Source License generally, please visit the Adopting and Developing Business Source License FAQ.

